# README #

Este README documenta as etapas necessárias para colocar seu aplicativo em funcionamento.

### Para que serve este repositório? ###

* Resumo rápido...
* Versão...
* [Aprenda Markdown] (https://bitbucket.org/tutorials/markdowndemo)

### Como faço para configurar? ###

* Resumo da configuração...
* Configuração...
* Dependências...
* Configuração de banco de dados...
* Como fazer testes...
* Instruções de implantação...

#### Documentação ####

Para gerar a configuração para o sphinx

`> sphinx-apidoc -F -f -A "nome do autor" -V 0 -R 0.1 -H "stand" -e -P -a --ext-autodoc --ext-viewcode --ext-todo -o . ./../src/`

### Diretrizes de contribuição ###

* Testes de escrita...
* Revisão de código...
* Outras diretrizes...

### Com quem devo falar? ###

* Proprietário ou administrador do repo...
* Outro contato da comunidade ou equipe...

