.. stand documentation master file, created by
   sphinx-quickstart on Tue Dec 15 11:17:39 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to stand's documentation!
=================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   io_ficheiros
   io_terminal
   main
   utilizadores
   veiculos


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
